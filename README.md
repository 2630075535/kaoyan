# kaoyan 1234

Website: https://kaoyan1234.com

Repository: https://gitlab.com/qaqland/kaoyan

# set-up

```
$ sudo apt install hugo git

$ git clone --recurse-submodules https://gitlab.com/qaqland/kaoyan.git

$ hugo serve

# Normally, it works.. but if you want to clone submodule(theme) manually:

$ git submodule update --init
```

# copyright

CC-BY-NC-ND

(exclude References/Assets)
